class OutOfRangeException(Exception):
    pass

ocjena = 0.0
try:
    ocjena = float(input("Unesite ocjenu {0.0 - 1.0}: "))
except:
    print("Pogreška niste unjeli broj.")

if ocjena < 0.0 or ocjena > 1.0:
    raise OutOfRangeException("Ocjena izvan intervala 0.0 - 1.0")

if ocjena >= 0.9:
    print("A")
elif ocjena >= 0.8:
    print("B")
elif ocjena >= 0.7:
    print("C")
elif ocjena >= 0.6:
    print("D")
else:
    print("F")

