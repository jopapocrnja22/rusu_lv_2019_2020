from statistics import mean
import sys

filename = input('Enter the file name: ')
try:
    file = open(filename)
except:
    print('File cannot be opened:', filename)
    exit()
arr = []
for line in file:
    if line.startswith("X-DSPAM-Confidence"):
        broj = line[20:-1]
        arr.append(float(broj))
print(mean(arr))
