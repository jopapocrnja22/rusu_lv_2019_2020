import sys

filename = input('Enter the file name: ')
try:
    file = open(filename)
except:
    print('File cannot be opened:', filename)
    exit()
mails = []
emails = []
hostnames = dict()
for line in file:
    if line.startswith("From"):
        mail = line.split()[1]
        mails.append(mail)
        email = mail.split("@")
        emails.append(email[1])
for em in emails:
    if em not in hostnames:
        hostnames[em] = 1
    else:
        hostnames[em] += 1

print("emails: ", emails[:10])
print()
print("dictionary entries: ", list(hostnames.items())[:10])