from tkinter import Y
import numpy as np
from matplotlib import pyplot as plt
from sklearn.linear_model import LogisticRegression
import matplotlib

def generate_data(n):
    
    #prva klasa
    n1 = int(n/2)
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1))); 
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = int(n - n/2)
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data  = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n)    
    data = data[indices,:]
    
    return data

np.random.seed(242)
train_data = generate_data(200)
print(train_data.shape)
np.random.seed(12)
test_data = generate_data(100)

logreg = LogisticRegression()
logreg.fit(train_data[:,:2], train_data[:,2])
y_pred = logreg.predict(test_data[:, :2])
print(logreg.coef_)
print(logreg.intercept_)
a = logreg.coef_ * train_data[:, :2] + logreg.intercept_

plt.figure(1)
plt.plot(train_data[:, :2], a)

plt.figure(2)
plt.scatter(train_data[:,0], train_data[:,1], c=train_data[:,2])

colors=["black", (0,1,0)]

plt.figure(3)

plt.scatter(test_data[:,0], test_data[:,1], c=y_pred == test_data[:,2], cmap=matplotlib.colors.ListedColormap(colors))


plt.show()
