import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
import tensorflow as tf
import seaborn as sns

def plot_confusion_matrix(c_matrix):

    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)
    fig = plt.figure("6. zadatak")
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')
    width = len(c_matrix)
    height = len(c_matrix[0])
    for x in range(width):
        for y in range(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x),
                            horizontalalignment='center',
                            verticalalignment='center',
                            color = 'green', size = 20)
    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])

    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()


# Model / data parameters
num_classes = 10
input_shape = (28, 28, 1)

# train i test podaci
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# prikaz karakteristika train i test podataka
print('Train: X=%s, y=%s' % (x_train.shape, y_train.shape))
print('Test: X=%s, y=%s' % (x_test.shape, y_test.shape))

# TODO: prikazi nekoliko slika iz train skupa

plt.figure(1, figsize=(14,7))
for i in range(15):
    plt.subplot(3,5,i+1)
    plt.imshow(x_train[i], cmap='gray', interpolation='none')
plt.show()

# skaliranje slike na raspon [0,1]
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

# slike trebaju biti (28, 28, 1)
x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)

print("x_train shape:", x_train_s.shape)
print(x_train_s.shape[0], "train samples")
print(x_test_s.shape[0], "test samples")


# pretvori labele
y_train_s = keras.utils.to_categorical(y_train, num_classes)
y_test_s = keras.utils.to_categorical(y_test, num_classes)


# TODO: kreiraj model pomocu keras.Sequential(); prikazi njegovu strukturu

model = keras.Sequential()
model.add(layers.Input(shape=input_shape))
model.add(layers.Flatten())
model.add(layers.Dense(units=64, activation='relu'))
model.add(layers.Dense(units=10, activation='softmax'))


# TODO: definiraj karakteristike procesa ucenja pomocu .compile()
history = model.compile(loss="categorical_crossentropy",
                        optimizer="adam",
                        metrics=["accuracy",])

# TODO: provedi ucenje mreze
history = model.fit(x_train_s, y_train_s, epochs=5, batch_size=32)
model.summary()


# TODO: Prikazi test accuracy i matricu zabune

eval = model.evaluate(x_test_s, y_test_s, batch_size=128)
print("loss, accuracy:", eval)
y_pred = model.predict(x_test_s, batch_size=128)
c_matrix = confusion_matrix(np.argmax(y_test_s, axis=1), np.argmax(y_pred, axis=1))
plot_confusion_matrix(c_matrix)


# TODO: spremi model

#model.save()


model_cnn = keras.Sequential([

    keras.Input(shape=input_shape),

    layers.Conv2D(filters=32, kernel_size=5, activation="relu", padding='same'),
    layers.MaxPool2D(),

    layers.Conv2D(filters=64, kernel_size=3, activation="relu", padding='same'),
    layers.MaxPool2D(),

    layers.Flatten(),
    layers.Dense(units=20, activation="relu"),
    layers.Dropout(0.5),
    layers.Dense(num_classes, activation="sigmoid"),
])

model_cnn.compile(loss="categorical_crossentropy",
                        optimizer="adam",
                        metrics=["accuracy",])

model_cnn.fit(x_train_s, y_train_s, epochs=5, batch_size=32)
model_cnn.summary()


eval = model_cnn.evaluate(x_test_s, y_test_s, batch_size=128)
print("loss, accuracy:", eval)
y_pred = model_cnn.predict(x_test_s, batch_size=128)
c_matrix = confusion_matrix(np.argmax(y_test_s, axis=1), np.argmax(y_pred, axis=1))
plot_confusion_matrix(c_matrix)

#model_cnn.save()
