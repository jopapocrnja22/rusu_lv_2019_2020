from operator import le
import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras.preprocessing import image_dataset_from_directory
import pandas as pd
from tensorflow import keras
from tensorflow.keras import layers
from sklearn.metrics import confusion_matrix

def plot_confusion_matrix(c_matrix):
    
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')

    width = len(c_matrix)
    height = len(c_matrix[0])

    for x in range(width):
        for y in range(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x), 
                        horizontalalignment='center',
                        verticalalignment='center', color = 'green', size = 20)

    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])
    
    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()


# ucitavanje podataka iz odredenog direktorija
train_ds = image_dataset_from_directory(
 directory='LVzadaci/rusu_lv_2019_2020/LV8/solutions/Train/',
 labels='inferred',
 label_mode='categorical',
 batch_size=32,
 image_size=(48, 48))

import os
# directory = "test_dir"
# parent_dir  = 'LVzadaci/rusu_lv_2019_2020/LV8/solutions'
# path = os.path.join(parent_dir, directory)
# os.mkdir(path)
# print("Directory '% s' created" % directory)

test_csv = pd.read_csv('LVzadaci/rusu_lv_2019_2020/LV8/solutions/Test.csv')
print(len(test_csv))
train_csv = pd.read_csv('LVzadaci/rusu_lv_2019_2020/LV8/solutions/Train.csv')

meta_csv = pd.read_csv('LVzadaci/rusu_lv_2019_2020/LV8/solutions/Meta.csv')

ClassId = meta_csv["ClassId"]

# for Id in ClassId:

    # directory = str(Id)
    # parent_dir  = 'LVzadaci/rusu_lv_2019_2020/LV8/solutions/test_dir'
    # path = os.path.join(parent_dir, directory)
    # os.mkdir(path)
    
import shutil


image_path = "LVzadaci/rusu_lv_2019_2020/LV8/solutions/Test"
for i in range(0, len(test_csv)):
    source="LVzadaci/rusu_lv_2019_2020/LV8/solutions/"+str(test_csv["Path"][i])
    num=str(test_csv["ClassId"][i])
    destination="LVzadaci/rusu_lv_2019_2020/LV8/solutions/test_dir/"+num
    dest = shutil.copy2(source, destination)

model = keras.Sequential()
model.add(keras.Input(shape=(48,48,3)))
model.add(layers.Conv2D(32, kernel_size=(3,3),padding='same', activation="relu"))
model.add(layers.Conv2D(32, kernel_size=(3,3), activation="relu"))
model.add(layers.MaxPool2D(pool_size=(2,2), strides=(2,2)))
model.add(layers.Dropout(rate=0.2))
model.add(layers.Conv2D(64, kernel_size=(3,3), padding='same', activation="relu"))
model.add(layers.Conv2D(64, kernel_size=(3,3), activation="relu"))
model.add(layers.MaxPool2D(pool_size=(2,2), strides=(2,2)))
model.add(layers.Dropout(rate=0.2))
model.add(layers.Conv2D(128, kernel_size=(3,3), padding='same', activation="relu"))
model.add(layers.Conv2D(128, kernel_size=(3,3), activation="relu"))
model.add(layers.MaxPool2D(pool_size=(2,2), strides=(2,2)))
model.add(layers.Dropout(rate=0.2))
model.add(layers.Flatten())
model.add(layers.Dense(units=512, activation='relu'))
model.add(layers.Dropout(rate=0.5))
model.add(layers.Dense(units=43, activation='softmax'))
model.summary()

model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

model.fit_generator(train_ds, epochs=5)

test_ds = image_dataset_from_directory(
 directory='LVzadaci/rusu_lv_2019_2020/LV8/solutions/test_dir/',
 labels='inferred',
 label_mode='categorical',
 batch_size=32,
 image_size=(48, 48))

predict = model.evaluate_generator(test_ds)
print(predict)

cmatrix=confusion_matrix(test_ds,predict)
plot_confusion_matrix(cmatrix)


