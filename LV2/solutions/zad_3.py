import numpy as np
from numpy.random import default_rng
import matplotlib.pyplot as plt

def makeGraph(data, color):
    plt.hist(data, color=color, density=True, edgecolor='k')
    plt.axvline(data.mean(), color='k', linestyle='dashed', linewidth=1.5)
    min_ylim, max_ylim = plt.ylim()
    plt.text(data.mean()*1.005, max_ylim*0.9, 'Mean: {:.2f}'.format(data.mean()))

genders = default_rng().integers(2, size=(1, 100))
maleHeight = np.random.normal(loc=180, scale=7, size=np.count_nonzero(genders == 1))
femaleHeight = np.random.normal(loc=167, scale=7, size=np.count_nonzero(genders == 0))

plt.subplot(1, 2, 1)
plt.title("Male height distribution")
makeGraph(maleHeight, 'b')

plt.subplot(1, 2, 2)
plt.title("Female height distribution")
makeGraph(femaleHeight, 'r')
plt.show()
