import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

img = mpimg.imread('tiger.png')

plt.subplot(1, 2, 1)
plt.imshow(img)

img *= 2
img = np.clip(img, 0, 1)
plt.subplot(1, 2, 2)
plt.imshow(img)
plt.show()
