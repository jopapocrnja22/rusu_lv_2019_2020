
import numpy as np
from numpy.random import default_rng
import matplotlib.pyplot as plt

dieRolls = default_rng().integers(low=1,high=7,size=100)

plt.hist(dieRolls, color='c', density=True, edgecolor='k', bins=np.arange(2, 14)/2)
plt.show()