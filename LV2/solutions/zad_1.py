import re

def removeAtSign(mailNames):
    names=[]
    for name in mailNames:
        names.append(name[0:name.find('@')])
    return names

fname = 'mbox-short.txt'
try:
    fhand = open(fname)
except:
    print ('File cannot be opened:', fname)
    exit()

lines=[]
for line in fhand:
    lines.append(line)

lineStrings = ''.join(str(e) for e in lines)
mails = re.findall(r'\b[a-zA-Z]+@[a-zA-z.]+\b',lineStrings)

mailsList = []
for mail in mails:
    if(mail not in mailsList):
        mailsList.append(mail)
mailsAsStrings = ' '.join(str(e) for e in mailsList)
mailNames= re.findall(r'\b\S+@',mailsAsStrings)

mailNames=removeAtSign(mailNames)
mailNamesString = ' '.join(str(e) for e in mailNames)
print(mailNames)