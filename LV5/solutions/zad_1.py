from tkinter import Y
from sklearn import datasets
from sklearn import cluster
from sklearn.cluster import KMeans
import numpy as np
from matplotlib import pyplot as plt
import sklearn

def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                          cluster_std=[1.0, 2.5, 0.5],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

for i in range (1,6):
    data = generate_data(500, i)
    clusters = KMeans(3).fit_predict(data)
    centers = KMeans(3).fit(data)
    plt.figure(1)
    plt.scatter(data[:,0], data[:,1], c=clusters)
    plt.scatter(centers.cluster_centers_[:,0],centers.cluster_centers_[:,1], c=(0,0,0))

    plt.show()

