import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error

def pravac(x, theta):
    return theta[1]*x + theta[0]

def kriterijJ(x,y,theta):
    J = 0.0
    n = x.shape[0]
    for i in range(0,n):
        J += (pravac(x[i],theta) - y[i]) ** 2
    
    J /= (2*n)
    return J

def non_func(x):
	y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
	return y

def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy

x = np.linspace(1,10,100)
y_true = non_func(x)
y_measured = add_noise(y_true)

plt.figure(1)
plt.plot(x,y_measured,'ok',label='mjereno')
plt.plot(x,y_true,label='stvarno')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)

np.random.seed(12)
indeksi = np.random.permutation(len(x))
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))]
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)]

x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]

xtrain = x[indeksi_train]
ytrain = y_measured[indeksi_train]

xtest = x[indeksi_test]
ytest = y_measured[indeksi_test]

plt.figure(2)
plt.plot(xtrain,ytrain,'ob',label='train')
plt.plot(xtest,ytest,'or',label='test')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)

linearModel = lm.LinearRegression()
linearModel.fit(xtrain,ytrain)

print('Model je oblika y_hat = Theta0 + Theta1 * x')
print('y_hat = ', linearModel.intercept_, '+', linearModel.coef_, '*x')

ytest_p = linearModel.predict(xtest)
MSE_test = mean_squared_error(ytest, ytest_p)

plt.figure(3)
plt.plot(xtest,ytest_p,'og',label='predicted')
plt.plot(xtest,ytest,'or',label='test')
plt.legend(loc = 4)

x_pravac = np.array([1,10])
x_pravac = x_pravac[:, np.newaxis]
y_pravac = linearModel.predict(x_pravac)
plt.plot(x_pravac, y_pravac)


X = np.ones((len(xtrain), 1))
X = np.append(X, xtrain, axis=1)
y_pred = np.linalg.inv(np.transpose(X) @ X ) @ np.transpose(X) @ ytrain
print(y_pred)

no_iter = 200
theta = np.zeros((2,1))
alfa = 0.02
J = np.zeros((no_iter,1))

for iter in range(0,no_iter):

    J[iter] = kriterijJ(xtrain,ytrain,theta)

    rj0 = 0.0
    rj1 = 0.0

    for i in range(0, len(xtrain)):
        rj0 += (theta[1] * xtrain[i] + theta[0]) - ytrain[i]
        rj1 += ((theta[1] * xtrain[i] + theta[0]) - ytrain[i]) * xtrain[i]
    
    rj0 /= len(xtrain)
    rj1 /= len(xtrain)

    theta[0] = theta[0] - alfa * rj0
    theta[1] = theta[1] - alfa * rj1

    print(J[iter])

plt.figure(4)
plt.plot(range(iter+1),J)
plt.ylabel("Vrijednost kriterijske funkcije")
plt.xlabel("iteracija")

print(theta)

plt.show()
