import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('mtcars.csv')

potrošnja = mtcars.sort_values(by=['mpg'], ascending=False)
print(potrošnja['car'].head(5))
print(potrošnja[mtcars.cyl == 8].car.tail(3))
print(potrošnja[mtcars.cyl == 6].mean().mpg)
print(potrošnja[(mtcars.cyl == 4) & (mtcars.wt > 2) & (mtcars.wt < 2.2)].mean().mpg)
print("automatski: ", mtcars[mtcars.am == 0].count().car)
print("rucni: ", mtcars[mtcars.am == 1].count().car)
print("automatski preko 100hp: ", mtcars[(mtcars.am == 0) & (mtcars.hp > 100)].count().car)
print("Car mass in kg: ", mtcars.wt * 0.4535923 * 1000)
