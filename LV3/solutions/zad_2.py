import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('mtcars.csv')

mpg4cyl = mtcars[mtcars.cyl == 4].mpg
mpg6cyl = mtcars[mtcars.cyl == 6].mpg
mpg8cyl = mtcars[mtcars.cyl == 8].mpg

plt.bar(4, mpg4cyl)
plt.bar(6, mpg6cyl)
plt.bar(8, mpg8cyl)
plt.show()

wt4cyl = mtcars[mtcars.cyl == 4].wt
wt6cyl = mtcars[mtcars.cyl == 6].wt
wt8cyl = mtcars[mtcars.cyl == 8].wt

data = []
data.append(wt4cyl)
data.append(wt6cyl)
data.append(wt8cyl)
plt.boxplot(data)
plt.show()

plt.bar(mtcars['am'], mtcars['mpg'])
plt.show()

plt.bar(mtcars['am'], mtcars.qsec/mtcars.hp)
plt.show()